var createError = require("http-errors")
var express = require("express")
var path = require("path")
var cookieParser = require("cookie-parser")
var logger = require("morgan")
var cors=require("cors")
const bodyParser = require("body-parser")// 解析body字段模块
const morgan = require("morgan") // 命令行log显示
const mongoose = require("mongoose")
const passport = require("passport")// 用户认证模块passport
// const Strategy = require("passport-http-bearer").Strategy// token验证模块
const foods = require("./routes/foods")
const products=require("./routes/products")
const regist=require("./routes/regist")
const log=require("./routes/log")
const search=require("./routes/search")
const userEssay=require("./routes/userEssay")
const order=require("./routes/order")
const comment=require("./routes/comment")
var indexRouter = require("./routes/index")
var usersRouter = require("./routes/users")
var app = express()

var mongodbUri= "mongodb+srv://leon:liang369369@wit-donation-cluster-lovf9.mongodb.net/foodhub?retryWrites=true&w=majority"
mongoose.connect(mongodbUri,{useNewUrlParser:true})

// const routes = require('./routes');
app.use(cors())
app.use(passport.initialize())// 初始化passport模块
app.use(morgan("dev"))// 命令行中显示程序运行日志,便于bug调试
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json()) // 调用bodyParser模块以便程序正确解析body传入值
app.get("/", (req, res) => {
  res.json({ message: "hello index!"})
})

app.use("/api", require("./routes/users")) // 在所有users路由前加/api

mongoose.Promise = global.Promise

app.use("/", indexRouter)
app.use("/users", usersRouter)
// view engine setup
app.set("views", path.join(__dirname, "views"))
app.set("view engine", "ejs")
if (process.env.NODE_ENV !== "test") {
  app.use(logger("dev"))
}
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, "public")))


app.get("/foods", foods.findAll)
app.get("/foods/:id", foods.findOne)
app.post("/foods",foods.addFood)
app.put("/foods/:id/likes", foods.incrementLikes)
app.delete("/foods/:id", foods.deleteFood)

app.get("/products", products.findAll)
app.get("/products/:id", products.findOne)
app.post("/products",products.addProduct)
app.put("/products/:id/likes", products.incrementLikes)
app.delete("/products/:id", products.deleteProduct)
app.post("/regist",regist.addUser)
app.post("/log",log.logUser)
app.post("/search",search.searchFood)
app.post("/order",order.addCart)
app.delete("/order/:id",order.deleteOrder)
app.get("/order",order.findAll)
app.get("/userEssay",userEssay.findAll)
app.get("/userEssay/:id",userEssay.findOne)
app.post("/userEssay",userEssay.addEssay)
app.put("/userEssay/:id/likes",userEssay.incrementLikes)
app.delete("/userEssay/:id",userEssay.deleteEssay)
app.post("/comment",comment.addComment)
// routes(app);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})
// error handler
app.use(function(err, req, res, ) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get("env") === "development" ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render("error")
})

module.exports = app
