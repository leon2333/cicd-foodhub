const chai = require("chai")
const server = require("../../../bin/www")
const expect = chai.expect
const request = require("supertest")
const _ = require("lodash")


describe("order", () => {

  describe("POST /order", () => {
    it("should return confirmation message and update ", () => {
      const order = {
        product:"noodels"
      }

      return request(server)
        .post("/order")
        .send(order)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .then(res => {
          const result = _.map(res.body,() => {
            return {
              product:"noodels"
            }
          })
          expect(res.body.message).equals("Order Successfully Added!")

          expect(result).to.deep.include({
            product:"noodels"
          })

        })
    })


  })
})
