"use strict";

var express = require("express");

var User = require("../models/user");

var jwt = require("jsonwebtoken");

var config = require("../config");

var passport = require("passport");

var router = express.Router();

var mongoose = require("mongoose");

require("../passport")(passport);

var mongodbUri = "mongodb+srv://leon:liang369369@wit-donation-cluster-lovf9.mongodb.net/foodhub?retryWrites=true&w=majority";
mongoose.connect(mongodbUri, {
  useNewUrlParser: true
});
mongoose.set("useFindAndModify", false); // let db = mongoose.connection

mongoose.set("useCreateIndex", true); //解决问题代码
// 注册账户

router.post("/signup", function (req, res) {
  if (!req.body.name || !req.body.password) {
    res.json({
      success: false,
      message: "请输入您的账号密码."
    });
  } else {
    var newUser = new User({
      name: req.body.name,
      password: req.body.password
    }); // 保存用户账号

    newUser.save(function (err) {
      if (err) {
        return res.json({
          success: false,
          message: "注册失败!"
        });
      }

      res.json({
        success: true,
        message: "成功创建新用户!"
      });
    });
  }
}); // 检查用户名与密码并生成一个accesstoken如果验证通过

router.post("/user/accesstoken", function (req, res) {
  User.findOne({
    name: req.body.name
  }, function (err, user) {
    if (err) {
      throw err;
    }

    if (!user) {
      res.json({
        success: false,
        message: "认证失败,用户不存在!"
      });
    } else if (user) {
      // 检查密码是否正确
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          var token = jwt.sign({
            name: user.name
          }, config.secret, {
            expiresIn: 10080 // token 过期销毁时间设置

          });
          user.token = token;
          user.save(function (err) {
            if (err) {
              res.send(err);
            }
          });
          res.json({
            success: true,
            message: "验证成功!",
            token: "Bearer " + token,
            name: user.name
          });
        } else {
          res.send({
            success: false,
            message: "认证失败,密码错误!"
          });
        }
      });
    }
  });
}); // passport-http-bearer token 中间件验证
// 通过 header 发送 Authorization -> Bearer  + token
// 或者通过 ?access_token = token

router.get("/user/user_info", passport.authenticate("bearer", {
  session: false
}), function (req, res) {
  res.json({
    username: req.user.name
  });
});
module.exports = router;