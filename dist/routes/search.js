"use strict";

var express = require("express");

var router = express.Router();

var mongoose = require("mongoose");

var Food = require("../models/foods");

var mongodbUri = "mongodb+srv://leon:liang369369@wit-donation-cluster-lovf9.mongodb.net/foodhub?retryWrites=true&w=majority";
mongoose.connect(mongodbUri); // let db = mongoose.connection

router.searchFood = function (req, res) {
  res.setHeader("Content-Type", "application/json");
  var reg = new RegExp(req.body.author, "i");
  Food.find({
    author: {
      $regex: reg
    }
  }, function (err, user) {
    if (err) {
      res.json({
        message: "User NOT Found!",
        errmsg: err
      });
    } else {
      res.send(JSON.stringify(user, null, 5));
    }
  });
};

module.exports = router;