"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var express = require("express");

var router = express.Router();

var mongoose = require("mongoose");

var Comment = require("../models/comment");

var Essay = require("../models/essay");

var mongodbUri = "mongodb+srv://leon:liang369369@wit-donation-cluster-lovf9.mongodb.net/foodhub?retryWrites=true&w=majority";
mongoose.connect(mongodbUri);
var db = mongoose.connection;
db.on("error", function (err) {
  console.log("Unable to Connect to [ " + db.name + " ]", err);
});
db.once("open", function () {
  console.log("Successfully Connected to [ " + db.name + " ]");
});

router.addComment =
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(req, res) {
    var comment;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            res.setHeader("Content-Type", "application/json");
            comment = new Comment();
            comment.author = req.body.author;
            comment.content = req.body.content;
            comment.essay_id = req.body.essay_id;
            _context.next = 7;
            return comment.save(function (err) {
              if (err) {
                return res.json({
                  message: "New comment NOT Added!",
                  errmsg: err
                });
              } else {
                Essay.findById(comment.essay_id, function (err, essay) {
                  if (err) return res.json({
                    message: "Fail to submit!",
                    errmsg: err
                  });else {
                    essay.comment.push({
                      content: comment.content,
                      comment_id: comment.id,
                      author: comment.author
                    });
                    essay.save(function (err) {
                      if (err) return res.json({
                        message: "Fail to submit!",
                        errmsg: err
                      });else return res.json({
                        message: "Submit your comment Successfully!",
                        data: essay
                      });
                    });
                  }
                });
              }
            });

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

module.exports = router;