"use strict";

var express = require("express");

var router = express.Router();

var mongoose = require("mongoose");

var Order = require("../models/order"); // var User= require("../models/register")


var mongodbUri = "mongodb+srv://leon:liang369369@wit-donation-cluster-lovf9.mongodb.net/foodhub?retryWrites=true&w=majority";
mongoose.connect(mongodbUri);
var db = mongoose.connection;
db.on("error", function (err) {
  console.log("Unable to Connect to [ " + db.name + " ]", err);
});
db.once("open", function () {
  console.log("Successfully Connected to [ " + db.name + " ]");
});

router.findAll = function (req, res) {
  // Return a JSON representation of our list
  res.setHeader("Content-Type", "application/json");
  Order.find(function (err, orders) {
    if (err) res.send(err);
    res.send(JSON.stringify(orders, null, 5));
  });
};

router.deleteOrder = function (req, res) {
  Order.findByIdAndRemove(req.params.id, function (err) {
    if (err) res.json({
      message: "Order NOT DELETED!",
      errmsg: err
    });else res.json({
      message: "Order  Successfully Deleted!"
    });
  });
};

router.addCart = function (req, res) {
  res.setHeader("Content-Type", "application/json");
  var order = new Order(); // order.customer_id= req.body.customer_id;

  order.product = req.body.product;
  order.save(function (err) {
    if (err) res.json({
      message: "Order NOT Added!",
      errmsg: err
    });else res.json({
      message: "Order Successfully Added!",
      data: order
    });
  });
};

module.exports = router;