"use strict";

var mongoose = require("mongoose");

var EssaySchema = new mongoose.Schema({
  author: String,
  title: String,
  content: String,
  comment: Array,
  date: Date,
  likes: Number
}, {
  collection: "essay"
});
module.exports = mongoose.model("Essay", EssaySchema);