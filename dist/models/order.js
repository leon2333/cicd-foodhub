"use strict";

var mongoose = require("mongoose");

var orderSchema = new mongoose.Schema({
  // customer_id:String,         //用户id
  product: String //商品列表

}, {
  collection: "order"
});
module.exports = mongoose.model("Order", orderSchema);